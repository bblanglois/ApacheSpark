package spark;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import com.google.common.collect.Iterables;
import scala.Tuple2;


public class PageRank {

	//configuration variable
	

	public static Double DEFAULT_RANKING = 1.0;
    public static Double DAMPING_FACTOR = 0.85;
    public static NumberFormat NF = new DecimalFormat("00");
    
	public static void main(String[] args) throws Exception {

	
		
		String inPath = null;
		String outPath = null;
		int numberOfIterations =2;
		
		//check parameter length
		if (args==null|| args.length!=4) {
			System.out.print("Missing parameter ! You have to input 3 parameter: Input file, Output File and Number of Iteration");
		}
		else {
			inPath = args[0];
			outPath= args[1];
			numberOfIterations = Integer.parseInt(args[2]);
			
		}

		long assessedDate = ISO8601.toTimeMS(args[3]);
		
		
        // delete output path if it exists already
        FileSystem fs = FileSystem.get(new Configuration());
        if (outPath!=null &&  fs.exists(new Path(outPath)))
        	fs.delete(new Path(outPath), true);
        
               
		 JavaSparkContext context = new JavaSparkContext(new SparkConf().setAppName("Page-Rank"));
		  
		 //custom input format to treat 14 lines record
		 context.hadoopConfiguration().set("textinputformat.record.delimiter","\n\n");
//		 context.getConf().set("spark.sql.shuffle.partitions","100");
		  	
		 JavaPairRDD<String,Iterable<String>> parseData = context.textFile(inPath ,1).mapToPair(
				 new PairFunction<String, String, String>() {

					@Override
					public Tuple2<String, String> call(String s) throws Exception {
						StringTokenizer eachRecord = new StringTokenizer(s,"\n");
						String articleTitle = null;
						String pageLinks = null;
						long revisionDate = 0;
						while (eachRecord.hasMoreTokens()) {
								
								String line = eachRecord.nextToken();
								
								//handle error 1: some records are separated by tab not space
								line = line.replaceAll("\t", " ");
																
								//if Line contains REVISION --> extract Article title and RevisionID
								if (line.contains("REVISION")) {			
									articleTitle = line.split(" ")[3].trim();
									revisionDate = ISO8601.toTimeMS(line.split(" ")[4].trim());
									
								}
								else if (line.contains("MAIN")) {
									pageLinks = line.substring(4).trim();													
								}
						   }
					   	
					   return new Tuple2<String, String>(articleTitle,revisionDate+" " + pageLinks); 
					}
					 
				}).groupByKey().cache();

				
		 
		
		 //write the output for testing
		//  parseData.saveAsTextFile(outPath+"/iteration-parseData/");
		
		
		 JavaPairRDD<String,Iterable<String>> links = parseData.flatMapToPair(
				 new PairFlatMapFunction<Tuple2<String,Iterable<String>>, String, String>() {

					@Override
					public Iterable<Tuple2<String, String>> call(Tuple2<String, Iterable<String>> s)  {
 
						List<Tuple2<String,String>> res =new ArrayList();
						String articleTitle = s._1;
						long latestRevisionDate = 0;
						long revionsionDate = 0;
						String links = null;
						//iterate all values
						String eachElement = null;
						for(String eachValue:s._2) {
							eachElement = eachValue.trim();
							//get index of revision date
							int firstIndex = eachElement.indexOf(" ",0);
							//Handle error 2:There are some records which don't have revision date
							if (firstIndex==-1)
								continue;
							
							revionsionDate = Long.parseLong(eachElement.substring(0,firstIndex));
							
							//get latest revision date and check with input date
//							iff (a)	the revision’s creation date C predates Y and 
//							(b) there is no other revision for the same article with a creation date C’ so that C<C’<Y.
							if (latestRevisionDate < revionsionDate && revionsionDate <= assessedDate) {
								latestRevisionDate = revionsionDate;
								links = eachElement.substring(firstIndex+1);	
							}
						}
																					
							//Handle Error 3: There are some articles which don't have information in MAIN element
							if (links!=null) {
								String [] pageList = links.split(" ");
								 
								//remove duplicated links
								HashSet<String> noRedundantLink = new HashSet<String>();
								
								for (String eachlink: pageList) {
									noRedundantLink.add(eachlink);
								}
								
								//filter out self-loop
								noRedundantLink.remove(articleTitle);
								
								for (String eachPage : noRedundantLink) {
									res.add(new Tuple2<String, String>(articleTitle, eachPage));
								}
							}
														
							return res;
							
							
						
					}
					 
				}).distinct().groupByKey().cache();
		 


	     //for debug
		 //links.saveAsTextFile(outPath+ "/iteration-links/");
		 
		 //Step 3: Process article title without outlinks based on valid links
		 JavaPairRDD<String,Double> ranks = links.values().flatMapToPair(
				new PairFlatMapFunction<Iterable<String>, String, Double>() {

					@Override
					public Iterable<Tuple2<String, Double>> call(Iterable<String> s) throws Exception {
						List<Tuple2<String, Double>> res =new ArrayList();
											
						for (String eachPage : s) {
							res.add(new Tuple2<String, Double>(eachPage, PageRank.DEFAULT_RANKING));
						}
						return res;
					}
					
				}).distinct().cache();
		 

		
		 //for debug
//		 ranks.saveAsTextFile(outPath+ "/iteration00");
		 
		//Step 5: Make iteration and calculate the Page score
		 for(int i=0;i<numberOfIterations;i++) {
			 
			 JavaPairRDD<String,Double> contribs = links.join(ranks).values().flatMapToPair(
				v -> {
				 List<Tuple2<String,Double>> res = new ArrayList<Tuple2<String,Double>>();
				 int urlCount = Iterables.size(v._1);
				 for (String eachPage: v._1) {
					 res.add(new Tuple2<String, Double>(eachPage,v._2/urlCount));
				 }
				 return res;
				});
			
			 
			 ranks = contribs.reduceByKey((a,b)->a+b).mapValues(v-> (1-PageRank.DAMPING_FACTOR) + v * PageRank.DAMPING_FACTOR);
//			 lastOutPath = outPath + "/iteration" + NF.format(i + 1);
//			 ranks.saveAsTextFile(lastOutPath);
		 }
		 

		 List<Tuple2<String, Double>> finalRanks = ranks.collect();
			System.out.println("finalRanks:");
			for(Tuple2<String, Double> pair: finalRanks) {
				System.out.println(pair._1() + ", " + pair._2());
			}

		 
		 context.close();
	} 
		
}
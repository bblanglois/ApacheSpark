###### This work is licensed under a Creative Commons [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)

## Introduction
The goal of this exercise is to get familiar with the design, implementation and performance testing of Big Data crunching tasks using Apache Spark. It requires to design and implement algorithms for parsing, filtering, projecting, and transforming data, over a relatively large dataset, executing your code on a shared cluster. 

The main task of this code is to implement a watered-down version of the PageRank algorithm that deals with a sample of the parsed  version of the complete Wikipedia edit history as of January 2008, a single large text file in a tagged multi-line format.  Each revision history record consists of 14 lines, each starting with a tag and containing a spacedelimited series of entries. More specifically,each record contains the following data/tags, one tag per line:

##### REVISION: revision metadata, consisting of:

* article_id: a large integer, uniquely identifying each page.
* rev_id: a large number uniquely identifying each revision.
* article_title: a string denoting the page’s title (and the last part of the URL of the
page).
* timestamp: the exact date and time of the revision, in ISO 8601 format; e.g., 13:45:00
UTC 30 September 2013 becomes 2013-09-12T13:45:00Z, where T separates the
date from the time part and Z denotes the time is in UTC.
* [ip:]username: the name of the user who performed the revision, or her DNS-resolved
IP address (e.g., ip:office.dcs.gla.ac.uk) if anonymous.
* user_id: a large number uniquely identifying the user who performed the revision, or
her IP address as above if anonymous.

##### CATEGORY: list of categories this page is assigned to.
##### IMAGE: list of images in the page, each listed as many times as it occurs.
##### MAIN, TALK, USER, USER_TALK, OTHER: cross-references to pages in other namespaces.
##### EXTERNAL: list of hyperlinks to pages outside Wikipedia.
##### TEMPLATE: list of all templates used by the page, each listed as many times as it occurs.
##### COMMENT: revision comments as entered by the revision author.
##### MINOR: a Boolean flag (0|1) denoting whether the edit was marked as minor by the author.
##### TEXTDATA: word count of revision's plain text.
##### An empty line, denoting the end of the current record. 

Exemple :  

    REVISION 4781981 72390319 Steven_Strogatz 2006-08-28T14:11:16Z SmackBot 433328  
    CATEGORY American_mathematicians  
    IMAGE  
    MAIN Boston_University MIT Harvard_University  
    Cornell_University  
    TALK  
    USER  
    USER_TALK  
    OTHER De:Steven_Strogatz Es:Steven_Strogatz  
    EXTERNAL http://www.edge.org/3rd_culture/bios/strogatz.html  
    TEMPLATE Cite_book Cite_book Cite_journal  
    COMMENT ISBN formatting &/or general fixes using  [[WP:AWB|AWB]]  
    MINOR 1  
    TEXTDATA 229  
    [empty line]




The algorithm to calculate the pagerank for this task is given by this formula :  

    PR(u)=0.15 + 0.85 * Sum(PR(v)/L(v)), ∀v: ∃(v,u) ∈S

where L(v) is the number of out-links of page v.







## Solution

Our program contains 4 transformation and 3 RDD

#### 1. The 1st transformation:  Read data from enwiki-file and extract data

**1.1 Description:**   The input is enwiki file which contains 14 lines for 1 record. Each line will have different information such as: REVISION, MAIN....What we do in this step include:
- Setting custom input format for Spark context in order to treat 14 lines as 1 record
- Extract required information such as Article Title, Revision Date from REVISION tag, outlinks from MAIN tag.
 
**1.2 Input:** hadoop file

**1.3 Output:** ArticleTitle, [RevisionDate Outlink1 Outlink2...])

Example of output:


    (PageC,[1138038684000 PageA PageB PageD PageE])
    (PageD,[1138038684000 PageC PageE])
    (PageB,[1137983976000 PageE])
    (PageE,[1138038684000 PageD])
    (PageA,[1133891087000 PageB, 1196963087000 PageB, 1228585487000 PageB PageD PageC])

#### 2. The second transformation: Getting valid data


**2.1 Description:** All validation for an article includes:
    - Get latest revision record for an article and compare its revision date to input date.
    - Put all the outlinks in a Hashset to remove duplication outlink.
    - Filter self-loop by removing the article tile if it appears in outlink Hashset.

**2.2 Input:** 

    ArticleTitle1, [RevisionDate1 Outlink1 Outlink2 Outlink3]
    ArticleTitle1, [RevisionDate2 Outlink1 Outlink2 Outlink3 Outlink2]

**2.3 Output:**   

    ArticleTitle1, [RevisionDate2 Outlink1 Outlink2 Outlink3] 

where RevisionDate2 is the latest date and < input date from user.

#### 3. The thrid transformation: Initializing the page rank and process with article without outlinks.


**3.1 Description:** To ensure that we have all articles (include article without outlinks):
- We get all articles in Outlink set
- Applying distinct () to ensure no duplication article title
- Put them in ranks RDD and init default value of Page Rank. 

**3.2 Input:** 

    PageA PageB
    PageC PageD PageE
    PageD PageE
    PageE PageA
  
  where PageB is article without outlinks.

**3.3  Output**

    ranks = 
    PageA 1.0
    PageB 1.0
    PageC 1.0
    PageD 1.0
    PageE 1.0

#### 4. The 4th transformation: Calculating Page rank in many iterations


**4.1 Description:** In this step, we calculate the contributed value of each outlink in each article and then reduce by key. After then, we apply the page rank formula and then assign this value to article title. 

**4.2 Input:**  

    (ArticleTitle , [Outlink1 Outlink2...] 1.0)

**4.3 Output:** 

    (ArticleTitle1 score1)
    (ArticleTitle2 score2) 	

## Interesting aspect
**1.** Performance: 
- We apply cache () for 3 RDD links and ranks because they are reused in iterations so that the performance of program is better.
- Treating with article without outlinks at the first try: we used LeftOuterJoin but then we change to FlatMapToPair for having better performance because we understand LeftOuterJoin is expensive.
  
**2.** Handling errors: There are many cases that we try to handle
- Extracting information from input file: Some records are separated by \t, rather than by " "
- Some articles don't have revision date value
- There are some articles which don't have information in MAIN element
- There are some articles which don't have valid date format.


## TESTING

We've already tried with different test cases :

**1.** Test case 1: enwiki-20080103-sample.txt and enwiki-20080103-largersample.txt

**2.** Test case 2: Many records of the same article have different revision date.

**3.** Test case 3: Self-loop page.

**4.** Test case 4: Page with duplicated outlinks.

**5.** Test case 5: Page with no information in MAIN element.

By Benjamin Langlois 2018 




   